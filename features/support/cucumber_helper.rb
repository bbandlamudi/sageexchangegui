require 'capybara/cucumber'
require 'rspec/expectations'

# Capybara.default_driver = :selenium  #disable default firefox

Capybara.run_server = true
Capybara.default_max_wait_time= 20

def switch_tab
  page.driver.browser.switch_to.window (page.driver.browser.window_handles.last)
end

def  print_error(message)
  LOGGER.error "#{ message } \n"
end

def scroll_down
  page.execute_script "window.scrollBy(0,10000)"
end

def print_info(message)
  LOGGER.info "#{ message }  \n"
end

def refresh_page
  page.evaluate_script 'window.location.reload()'
end
