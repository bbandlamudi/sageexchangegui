module AsyncSupport
  def eventually
    timeout = 30
    polling_interval = 0.5
    time_limit = Time.now + timeout
    loop do
      sleep polling_interval
      begin
        yield
      rescue Exception => error
      end
      return if error.nil?
      raise error if Time.now >= time_limit
    end
  end
end

World(AsyncSupport)