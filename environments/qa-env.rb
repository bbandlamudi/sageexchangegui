userOptions = ENV['USER_OPTIONS']

# Load the default values from cucumber.yml profile if present
if !ENV['PROJECT_ID'].nil?
  projectId = ENV['PROJECT_ID']
end

if !ENV['TEST_USER'].nil?
  TEST_USER = ENV['TEST_USER']
end