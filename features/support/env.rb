require 'capybara'
require 'capybara/cucumber'
require 'cucumber'
require 'selenium-webdriver'
require 'minitest'
require 'faker'

#### Capybara initialization #######
def get_browser
  Capybara.default_driver = :selenium
  Capybara.register_driver :selenium do |app|
    @driver= Capybara::Selenium::Driver.new app, browser: :chrome
    #caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {"args" => [ "--start-maximized" ]})
    caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {"args" => %w{ window-size=1800,1000 }})
    @driver = Capybara::Selenium::Driver.new(app, {:browser => :chrome, :desired_capabilities => caps})
  end
end



Capybara.default_max_wait_time= 20

#### For headless execution #######
if ENV['HEADLESS'] == 'true'
  require 'headless'

  headless = Headless.new
  headless.start
  at_exit do
    headless.destroy
  end

end


def scroll_to(element)
  script = <<-JS
      arguments[0].scrollIntoView(true);
  JS

  Capybara.current_session.driver.browser.execute_script(script, element.driver)
end

#### Minitest initialization #######
module MiniTestAssertions
  def self.extended(base)
    base.extend(MiniTest::Assertions)
    base.assertions = 0
  end
  attr_accessor :assertions
end
World(MiniTestAssertions)


#pass environment variables to control which browser is used for testing.
#usage: firefox=true bundle exec cucumber features/test.feature


if ENV['chrome']
  Capybara.default_driver = :chrome
  Capybara.register_driver :chrome do |app|
    options = {
        :js_errors => false,
        :timeout => 360,
        :debug => false,
        :inspector => false,
    }
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
elsif ENV['firefox']
  Capybara.default_driver = :firefox
  Capybara.register_driver :firefox do |app|
    options = {
        :js_errors => true,
        :timeout => 360,
        :debug => false,
        :inspector => false,
    }
    Capybara::Selenium::Driver.new(app, :browser => :firefox)
  end
elsif ENV['safari']
  Capybara.default_driver = :safari
  Capybara.register_driver :safari do |app|
    options = {
        :js_errors => false,
        :timeout => 360,
        :debug => false,
        :inspector => false,
    }
    Capybara::Selenium::Driver.new(app, :browser => :safari)
  end
end