# This function login to the Sage Exchange portal as test user
# input: get the URL, user and password from environment file

##################################################################


def loginToSE(portal, user)
  func_name = "loginToSE"
  get_browser
  case portal
    when "QASageExchange"
      visit QASageExchange
    when "DevSageExchange"
      visit DevSageExchange
    when "VT4"
      visit VT4

    else
      puts "Please speicfy which portal to login"
  end
  case user
    when "QASETestAccountUser"
      $username = QASETestAccountUser[:username]
      fill_in('txtEmailAddress', :with => $username)
      fill_in('txtPassword', :with => QASETestAccountUser[:password])
      click_on('btnSignIn')

    when "DevSETestAccountUser"
      $username = DevSETestAccountUser[:username]
      fill_in('txtEmailAddress', :with => $username)
      fill_in('txtPassword', :with => DevSETestAccountUser[:password])
      click_on('btnSignIn')
    else
      puts "Please speicfy a user to login"
  end
end