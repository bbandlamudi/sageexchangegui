
And(/^I create a credit card transaction for Credit "([^"]*)"$/) do |credit_type|
  sleep(2)
  click_link("Credit Card")
  sleep(3)
  select(credit_type, :from => "ctl00$cphMain$ddlTransactionType")

end

And(/^I enter credit card transaction Information "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/) do |ordernumber, cardtype, expmonth, expyear, cvv, tax, subtotal|
  fill_in "cphMain_txtOrderId", :with => Faker::Number.number(10)
  fill_in "cphMain_txtCardnumber", :with => cardtype
  select(expmonth, :from=> "cphMain_ddlExpirationMonth")
  select(expyear, :from=> "cphMain_ddlExpirationYear")
  fill_in "cphMain_txtCVV" , :with => cvv
  fill_in "cphMain_txtSubtotal" , :with => subtotal
  fill_in "cphMain_txtTax" , :with => tax
  print_info("Card Number submitted")
end



And(/^I enter credit card transaction Information with custom ordernumber "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/) do |ordernumber, cardtype, expmonth, expyear, cvv, tax, subtotal|
  fill_in "cphMain_txtOrderId", :with => ordernumber
  fill_in "cphMain_txtCardnumber", :with => cardtype
  select(expmonth, :from=> "cphMain_ddlExpirationMonth")
  select(expyear, :from=> "cphMain_ddlExpirationYear")
  fill_in "cphMain_txtCVV" , :with => cvv
  fill_in "cphMain_txtSubtotal" , :with => subtotal
  fill_in "cphMain_txtTax" , :with => tax
  print_info("Card Number submitted")
end

And(/^I enter credit card Order Information "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/) do |name1, custno, address, city, state, zip, country|
  fill_in "cphMain_txtName" , :with => Faker::Name.name
  fill_in "cphMain_txtCustomerNumber" , :with => custno
  fill_in "cphMain_txtAddress", :with => address
  fill_in "cphMain_txtCity", :with => city
  fill_in "cphMain_txtZip", :with => zip
  select(state,:from=>"cphMain_ddlState")
  select(country,:from=>"cphMain_ddlCountry")
  scroll_down
  click_on("Next")
  print_info("Order Info Submitted")
end

And(/^I enter credit card Billing Information same as Order Information$/) do
  check "ctl00$cphMain$cbShippingAddress"
  scroll_down
  click_on("Next")
end

Then(/^I click on Bill tab to see the error$/) do
  click_link("Bill To")
  sleep(2)
  print_info("Error message displayed")
end

And (/^I click on Quick Search with OrderNumber$/) do
  #switch_tab
  click_link("Quick Search")
  fill_in "cphMain_txtOrdernumber", :with => @orderid
  click_on "Search"
  sleep 2

  end


And (/^I store order number for searching records$/) do
  @orderid = page.find(:xpath,"//*[@id='cphMain_lblResponseOrderNumValue']").text
  puts @orderid
end


Then(/^I verify if the reference number is unique and have (\d+) characters$/) do |ten|
  click_link ("Date")
  click_link ("Date")
  find(:xpath, "//*[@id='cphMain_grvSearchResults_hlView2_0']").click
  @refid= page.find(:xpath,"//*[@id='cphMain_grvSearchResults_lblReferenceVal_0']").text
  puts @refid
  length = @refid.size
  puts length
 end

And(/^I enter Order number more than (\d+) characters$/) do |arg|
  fill_in "cphMain_txtOrderId", :with => Faker::Number.number(arg)
end


Then(/^I set body to "(.+)" and "(.+)"$/) do |json, auth|
  i = 0
  loop do
    i+= 1

    table= find_by_id("parm-Transactions_Transactions_Post")
    within(table) do
      fill_in "request", :with => json
      fill_in "Authorization", :with => auth
    end
    page.execute_script "window.scrollBy(0,500)"
    table2 = find_by_id("test-Transactions_Transactions_Post")
    within(table2) do
      click_on('Try')
      sleep (3)
      find(:xpath, "//*[@id='modal-Transactions_Transactions_Post']/div/div/div[1]/button").click
      break if i >=2
    end
  end
end