
####### DEFINE LOGGER ########
log_dir = "./logs"
log_file = "#{log_dir}/se-tests.log"
Dir.mkdir(log_dir) unless File.exists?(log_dir)
File.delete(log_file) if File.exist?(log_file)
LOGGER   = Logger.new(log_file,5,10240000) # logfile roles over after 10MB. Max of 5 files kept
LOGGER.level = Logger::DEBUG
LOGGER.formatter = proc do | severity,datetime,progname,msg |
  current_time = Time.now.strftime("%Y-%m-%d %H:%M:%S:%L")
  pid = Process.pid
  if [nil,''].include?(Thread.current[:name]) then t_id = Thread.current.object_id
  else t_id = Thread.current[:name] end
  "[#{current_time} #{pid} #{t_id}] #{severity} : #{msg}\n"

end
LOGGER.info "#############  New SageExchange Test Execution  ########################"


After do |scenario|
  if scenario.failed?
    print_error(" Scenario Failed :  #{scenario.exception.message } \n")
    Dir::mkdir('screenshots') if not File.directory?('screenshots')
    screenshot = "./screenshots/FAILED_#{scenario.name.gsub(' ','_').gsub(/[^0-9A-Za-z_]/, '')}.png"
    # switch_tab
    page.driver.browser.save_screenshot(screenshot)
    sleep(3)
    embed screenshot, 'image/png'
  end
 # page.driver.browser.close
  Capybara.current_session.driver.quit
end




