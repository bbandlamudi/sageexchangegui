Feature: SageExchange - Validations on bankcard transactions for reference numbers and Order Number


  Scenario Outline: Verify Reference numbers are generated not more than 10 char's and no duplicates found
    Given I login to "QASageExchange" as "QASETestAccountUser"
    When I click on Virtual Terminal to select "<TestAccount>"
    When I click on Virtual Terminal to select "<ACHAccount>"
    Then I create a credit card transaction for Credit "<Type>"
    And I enter credit card transaction Information "<OrderNumber>" "<CardNumber>" "<ExpMonth>" "<ExpYear>" "<CVV>" "<tax>" "<Subtotal>"
    And I enter credit card Order Information "<Name>" "<CustNumber>" "<Address>" "<City>" "<State>" "<ZipCode>" "<Country>"
    And I enter Billing Information same as Order Information
    And I enter notes to submit transaction
    And I store order number for searching records
    Then I should see text "APPROVED"
    And I click on Quick Search with OrderNumber to find reference number
    Then I verify if the reference number is unique and have 10 characters

    Examples:
      | TestAccount                 |OrderNumber |CardNumber       |ExpMonth |ExpYear  |CVV       |Type    |Name          |Address             |City      |State|ZipCode       |Country        |Subtotal   | tax  | CustNumber|
      | Bharti Test Account for QA  |            |5499740000000057  |12       | 2020    | 998     |Credit  |              |test1234 automation |Ashburn   | VA  | 85284-3456   |United States  |  11.10     | 0  | 123456789 |
      | Bharti Test Account for QA  |            |5499740000000057  |12       | 2020    | 998     |Sale    |              |test1234 automation |Ashburn   | VA  | 85284        |United States  |  9.10     | 0  | 123456789 |
      | Bharti Test Account for QA  |            |5499740000000057  |12       | 2020    | 998     |Auth Only |            |test1234 automation |Ashburn   | VA  | 85284        |United States  |  10.10     | 0  | 123456789 |



  Scenario Outline: Verify Order Number with 40 char's
    Given I login to "QASageExchange" as "QASETestAccountUser"
    When I click on Virtual Terminal to select "<TestAccount>"
    When I click on Virtual Terminal to select "<ACHAccount>"
    Then I create a credit card transaction for Credit "<Type>"
    And I enter credit card transaction Information "<OrderNumber>" "<CardNumber>" "<ExpMonth>" "<ExpYear>" "<CVV>" "<tax>" "<Subtotal>"
    And I enter Order number more than 40 characters
    And I enter credit card Order Information "<Name>" "<CustNumber>" "<Address>" "<City>" "<State>" "<ZipCode>" "<Country>"
    And I enter Billing Information same as Order Information
    And I enter notes to submit transaction
    And I store order number for searching records
    Then I should see text "APPROVED"
    And I click on Quick Search with OrderNumber
    Then I verify ordernumber with 40 characters

    Examples:
      | TestAccount                 |OrderNumber |CardNumber       |ExpMonth |ExpYear  |CVV       |Type    |Name          |Address             |City      |State|ZipCode       |Country        |Subtotal   | tax  | CustNumber|
      | Bharti Test Account for QA  |            |5499740000000057  |12       | 2020    | 998     |Credit  |              |test1234 automation |Ashburn   | VA  | 85284-3456   |United States  |  11.10     | 0  | 123456789 |
      | Bharti Test Account for QA  |             |5499740000000057  |12       | 2020    | 998     |Sale  |             |test1234 automation |Ashburn   | VA  | 85284-3456   |United States  |  11.10     | 0  | 123456789 |



@OrderNumber_41Char
  Scenario Outline: Order Number with 41 char's should throw error
    Given I login to "QASageExchange" as "QASETestAccountUser"
    When I click on Virtual Terminal to select "<TestAccount>"
    When I click on Virtual Terminal to select "<ACHAccount>"
    Then I create a credit card transaction for Credit "<Type>"
    And I enter credit card transaction Information "<OrderNumber>" "<CardNumber>" "<ExpMonth>" "<ExpYear>" "<CVV>" "<tax>" "<Subtotal>"
    And I enter Order number more than 41 characters
    And I enter credit card Order Information "<Name>" "<CustNumber>" "<Address>" "<City>" "<State>" "<ZipCode>" "<Country>"
    And I enter Billing Information same as Order Information
    And I enter notes to submit transaction
    Then I should see error "Order ID: The order id field contains invalid characters"


  Examples:
      | TestAccount                 |OrderNumber |CardNumber       |ExpMonth |ExpYear  |CVV       |Type    |Name          |Address             |City      |State|ZipCode       |Country        |Subtotal   | tax  | CustNumber|
      | Bharti Test Account for QA  |             |5499740000000057  |12       | 2020    | 998     |Credit  |             |test1234 automation |Ashburn   | VA  | 85284-3456   |United States  |  11.10     | 0  | 123456789 |
      | Bharti Test Account for QA  |             |5499740000000057  |12       | 2020    | 998     |Sale  |             |test1234 automation |Ashburn   | VA  | 85284-3456   |United States  |  11.10     | 0  | 123456789 |

