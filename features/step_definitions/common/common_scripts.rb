#Common, generic steps used across multiple features can be stored here


Given(/^I login to "([^"]*)" as "([^"]*)"$/) do |portal, user|

  loginToSE(portal, user)
  end

And(/^I click on Virtual Terminal to select "([^"]*)"$/) do |account|
  sleep (5)
  switch_tab
  sleep (5)
  expect(page).to have_content("Transaction Information")
end


#Click a button with specified text
Given(/^I click on the "([^"]+)" button$/) do |target|
  click_on(target) # express the regexp above with the code you wish you had
end

Then(/^I should see error "(.+)"$/) do |text|
   if page.respond_to? :should
    expect(page).to have_content(text)
  else
    assert page.has_content?(text) # has_content? will never fail the test ,just silently return false and continue on
  end
end


#Page should or should not have the specified text
Then(/^I should (not )?see text "(.+)"$/) do |see, text|
  case see
    when "not "
      switch_tab
      eventually {expect(page).not_to have_content(text)}
      click_on("OK")
    else
      switch_tab
      eventually { expect(page).to have_content (text) }
      #expect(page).to have_content(text)
      click_on("OK")

  end
end

Given (/^I navigate to "([^"]*)"$/) do |portal|
  get_browser
  visit portal
end



Then(/^I should see the error message "(.+)"$/) do |message|
  expect(page).to have_content(message)
end




