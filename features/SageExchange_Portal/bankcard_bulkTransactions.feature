Feature: Bulk tests

  Scenario Outline: Create BULK Sale Transactions for set of card numbers, amount for new FrontEnd Communicator to Tsys

    Given I navigate to "https://dev.sagepayments.net/web_services/PaymentGateway/Bankcard#!/Transactions/Transactions_Post"
    Then I set body to "{"transactionId": "5656", "retail": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",    "cvv":"<cvv>",   "expiration": "1220"    },},     "transactionCode": "<txnCode>"}" and "Bearer SPS-DEV-GW:test.872254343769.12345"
   # Then I set body to "{"transactionId": "5656", "retail": {   "amounts": {      "total": <amount>    },   "authorizationCode": "565656",     "orderNumber": "5656",    "cardData": {   "number": "<cardNum>",      "expiration": "1220"    },"billing":{"zip":"85284"}},     "transactionCode": "<txnCode>"}" and "Bearer SPS-DEV-GW:test.872254343769.12345"


    Examples:

      | txnCode | amount   | cardNum               | cvv     |
      | Sale    | 12.00    | 6011000993026909      | 996     |
      | Sale    | 7.15     | 2223000048400011      | 998     |
      | Sale    | 0.52     | 4012000098765439      | 999     |
      | Sale    | 0.53     | 5499740000000057      | 998     |
      | Credit    | 15.00  | 6011000993026909  | 996     |
      | Credit    | 8.00  | 4012000098765439 | 999        |
    