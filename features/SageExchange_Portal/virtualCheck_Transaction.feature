Feature: SageExchange Virtual Check Transaction
# Test Virtual Check transactions


  Scenario Outline: Void ACH transaction and verify the Void status
    Given I login to "QASageExchange" as "QASETestAccountUser"
    When I click on Virtual Terminal to select "<ACHAccount>"
    Then I create a Virtual Check transaction for Credit "<Type>"
    And I enter transaction information "<Routing>" "<Account>" "<OrderNumber>" "<Subtotal>"
    And I enter Order Information "<FirstName>" "<LastName>" "<Address>" "<City>" "<State>" "<ZipCode>" "<Country>"
    And I enter Billing Information same as Order Information
    And I enter notes to submit transaction
    And I store order number for searching records
    Then I should see text "APPROVED"
    And I click on Quick Search with OrderNumber
    When I VOID Transaction
    Then transaction should be voided sucesfully for "<Subtotal>"

    Examples:
      |ACHAccount                     |Routing       |Account  |Type    |FirstName| LastName      |Address        |City   |State|ZipCode|Country       |Subtotal | OrderNumber | CustNumber|
      |   Bharti Test Account for QA  | 490000018 | 24413815     |Credit  |       |               |test1234 automation |Ashburn| VA    | 20148  |United States |  10     | 123456789  | 123456789 |
      |  Bharti Test Account for QA   |490000018 | 24413815     |Sale |           |               | address123 | Ashburn     | VA | 20148 | United States | 12 |  123456789  | 123456789   |
     # | Bharti Test Account for QA    |490000018  |24413815     |Auth Only    |Auth1     |Auth2   | test 123  | Ashburn   | VA | 20148  | United States | 15 | 123456789  | 123456789   |










