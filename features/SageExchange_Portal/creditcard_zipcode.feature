Feature: SageExchange - Validations for Zipcode

  @positive
  Scenario Outline: Verify Sage Exchange credit transaction with ZIP code hypen validations
    Given I login to "QASageExchange" as "QASETestAccountUser"
    When I click on Virtual Terminal to select "<ACHAccount>"
    Then I create a creadit card transaction for Credit "<Type>"
   # And I enter credit card transaction Information "<CardNumber>" "<ExpMonth>" "<ExpYear>" "<CVV>" "<tax>" "<Subtotal>" "<OrderID>"
   # And I enter credit card Order Information "<Name>" "<CustNumber>" "<Address>" "<City>" "<State>" "<ZipCode>" "<Country>"
    #And I enter credit card transaction Information "<CardNumber>" "<ExpMonth>" "<ExpYear>" "<CVV>" "<tax>" "<Subtotal>"
    And I enter credit card transaction Information "<OrderNumber>" "<CardNumber>" "<ExpMonth>" "<ExpYear>" "<CVV>" "<tax>" "<Subtotal>"
    And I enter credit card Order Information "<Name>" "<CustNumber>" "<Address>" "<City>" "<State>" "<ZipCode>" "<Country>"
    And I enter Billing Information same as Order Information
    And I enter notes to submit transaction
    Then I should see text "APPROVED"

    Examples:
      | ACHAccount                  |OrderNumber  |CardNumber       |ExpMonth |ExpYear  |CVV       |Type    |Name        |Address             |City      |State|ZipCode       |Country        |Subtotal | tax  |OrderID | CustNumber|
      | Bharti Test Account for QA  |1234567890   |5499740000000057  |12       | 2020    | 998     |Credit  |card1 card2 |test1234 automation |Ashburn   | VA  | 85284-3456   |United States  |  11.10     | 0  | 123456789  | 123456789 |
      | Bharti Test Account for QA  |1234567890   |4012000098765439  | 12      | 2020    | 999     |Credit  |card1 card2 |test1234 automation |Ashburn   | VA  | 85284 3456   |United States  | 11.12     |  0  | 123456789  | 123456789 |
    #  | Bharti Test Account for QA  |4012000098765439 |12       | 2020    |  999    |Credit  |card1 card2  |test1234 automation |Ashburn   | VA  | 20148 1234  |United States  |  12     | 11    | 123456789  | 123456789 |
    #  | Bharti Test Account for QA  |4012000098765439 | 2020    |      |Credit  |card1 card2  |test1234 automation |Ashburn   | VA  | 20148 123  |United States  |  13     | 11     | 123456789  | 123456789 |


 # @negative
  #Scenario Outline: Verify Negative credit transaction with ZIP code hypen validation
   # Given I login to "SageExchange" as "TestAccoutUser"
   # When I click on Virtual Terminal to select "<ACHAccount>"
   # Then I create a creadit card transaction for Credit "<Type>"
   # And I enter credit card transaction information "<CardNumber>" "<ExpYear>" "<tax>" "<Subtotal>" "<OrderID>"
   # And I enter credit card Order Information "<Name>" "<CustNumber>" "<Address>" "<City>" "<State>" "<ZipCode>" "<Country>"
   #And I enter Billing Information same as Order Information
   # And I enter notes to submit transaction
   # And I click on Bill tab to see the error
   # Then I should see error "Zip Code: Invalid zip code"

   # Examples:
    #  | ACHAccount                  |CardNumber       |ExpYear  |CVV  |Type    |Name   |Address        |City          |State|ZipCode   |Country       |Subtotal | tax  |OrderID | CustNumber|
     # | Bharti Test Account for QA  |4012000098765439 | 2020    | 998     |Credit  |card1 card2  |test1234 automation |Ashburn   | VA  | 20148 12345  |United States  |  14     | 11     | 123456789  | 123456789 |
