
And(/^I create a Virtual Check transaction for Credit "([^"]*)"$/) do |credit_type|
  click_link("Virtual Check")
  sleep(3)
  select(credit_type, :from => "ctl00$cphMain$ddlTransactionType")
  print_info(credit_type)
end

And(/^I enter transaction information "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/) do |routing,account, order, subtotal|
  fill_in "cphMain_txtRte" , :with => routing
  fill_in "cphMain_txtAccountNumber", :with => account
  fill_in "cphMain_txtOrderId" , :with => Faker::Number.number(10)
  fill_in "cphMain_txtSubtotal" , :with => subtotal
  print_info("Transaction Information")
end

And(/^I enter Order Information "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/) do |firstname, lastname,address, city, state, zip, country|
 # fill_in "cphMain_txtName" , :with => firstname
  fill_in "cphMain_txtName" , :with => Faker::Name.first_name
  fill_in "cphMain_txtLastName" , :with => Faker::Name.last_name
#  fill_in "cphMain_txtLastName" , :with => lastname
  fill_in "cphMain_txtAddress", :with => address
  fill_in "cphMain_txtCity", :with => city
  fill_in "cphMain_txtZip", :with => zip
  select(state,:from=>"cphMain_ddlState")
  select(country,:from=>"cphMain_ddlCountry")
  click_on("Next")
  print_info("Order Information Submitted")
  puts @firstname
end

And(/^I enter Billing Information same as Order Information$/) do
  check "ctl00$cphMain$cbShippingAddress"
  scroll_down
  click_on("Next")
  print_info("Billing Information Submitted")
end


Then(/^I enter notes to submit transaction$/) do
  fill_in "cphMain_txtNote" , :with => "Automation-Test"
  scroll_down
  click_on("Process Transaction")
  sleep(5)

end

Then (/^I extract order number to search transaction$/) do
  table = find_by_id("cphMain_lblResponseOrderNumValue")
  within(table) do
    tr_all = page.all("tr")
    tr_all.each do |tr|
      td = tr.all("td")[2]
      puts td
    end
  end
end

Then(/^I VOID Transaction$/) do
  switch_tab
  click_link("Quick Search")
  fill_in "cphMain_txtOrdernumber", :with => @orderid
  click_on "Search"
  sleep 2
  find(:xpath, "//*[@id='cphMain_grvSearchResults_hlView2_0']").click
  @refid= page.find(:xpath,"//*[@id='cphMain_grvSearchResults_lblReferenceVal_0']").text
  @first_name= page.find(:xpath,"//*[@id='cphMain_grvSearchResults_lblName_0']").text
  find(:xpath, "//*[@id='cphMain_grvSearchResults_lbtnVoid_0']").click
  scroll_down
  click_on ("Yes") #Popup warning message to void a transaction
  sleep(5)
print_info("Extracted Order Number")
end



Then (/^transaction should be voided sucesfully for "([^"]*)"$/) do |total|
  refresh_page
  click_link("Quick Search")
  fill_in "cphMain_txtReference", :with => @refid
  click_on "Search"
  sleep (2)
  if page.has_content?("Void")
    expect(page).to have_content (total)
    expect(page).to have_content (@first_name)
  else
    page.has_content?("No search results.")
    loop do
      click_link("Quick Search")
      puts "Waiting for VOID transaction"
      fill_in "cphMain_txtReference", :with => @refid
      click_on "Search"
      if page.has_content?("Void")
        expect(page).to have_content (total)
        expect(page).to have_content (@first_name)
        break
      end
    end
  end

  print_info("Searched by Order Number and Verified Void Transaction")
end


